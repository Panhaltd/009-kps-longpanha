import React, { Component } from 'react'
import './App.css'

export default class App extends Component {
    constructor(){
        super();
        this.state = {
          hide:"Hide",
          unhide:"Unhide",
          foods: [
            {
              id: 1,
              food: "Pizza",
              price: 20,
              isHiding: false,
            },
            {
              id: 2,
              food: "Buger",
              price: 10,
              isHiding: false,
            },
            {
              id: 3,
              food: "Coca",
              price: 5,
              isHiding: false,
            },
            {
              id: 4,
              food: "Chicken",
              price: 25,
              isHiding: false,
            },
          ],
       };
      }

    changeColor=(i)=>{
        let newArr = this.state.foods
        newArr[i.id-1].isHiding=!newArr[i.id-1].isHiding
        this.setState({
            foods:newArr
        })
    }

    render() {
        var row = this.state.foods.map((items)=>
            <tr key={items.id} style={items.isHiding?{backgroundColor:'cadetblue'}:{backgroundColor:''}}>
                <td style={{padding: 10}}>{items.id}</td>
                <td style={{padding: 10}}>{items.food}</td>
                <td style={{padding: 10}}>{items.price}$</td>
                <td><button style={items.isHiding?{backgroundColor:'blue',color:'white'}:{backgroundColor:'black',color:'white'}} onClick={()=>this.changeColor(items)} >{items.isHiding?this.state.unhide:this.state.hide}</button></td>
            </tr>
        )
        return (
            <div>
                <h1>Food List</h1>
                <table>
                <tr>    
                    <th style={{padding: 10}}>Id</th>
                    <th style={{padding: 10}}>Price</th>
                    <th style={{padding: 10}}>Price</th>
                    <th style={{padding: 10}}>Actions</th>
                </tr>
                {row}
                </table>
            </div>
        )
    }
}
